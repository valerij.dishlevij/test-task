from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin
from rest_framework import routers
from test import views
from django.conf import settings

router = routers.DefaultRouter()
router.register(r'items', views.ItemsView)
router.register(r'upload_csv', views.FileUploadView)

urlpatterns = patterns('',
    url(r'^api/', include(router.urls)),
    url(r'^api/record/(?P<date_upload>\d{4}-\d{2}-\d{2})/$', views.RecordByDateView.as_view({'get': 'list'})),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^docs/', include('rest_framework_swagger.urls')),
)


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
