from test.models import TestData, CSVTable
from rest_framework import serializers


class ItemSerializer(serializers.HyperlinkedModelSerializer):
    active = serializers.BooleanField()
    serial_number = serializers.CharField(read_only=True)
    description = serializers.CharField(read_only=True)
    date = serializers.CharField(read_only=True)
    csv_file = serializers.CharField(read_only=True)

    class Meta:
        model = TestData
        fields = ('serial_number', 'description', 'active', 'date', 'csv_file',)


class ItemListSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TestData
        fields = ItemSerializer.Meta.fields


class FileUploadSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CSVTable
        read_only_fields = ('date_upload',)


class RecordByDateSerializer(serializers.HyperlinkedModelSerializer):
    date_upload = serializers.ReadOnlyField()

    class Meta:
        model = TestData
        fields = ItemSerializer.Meta.fields + ('date_upload', )
