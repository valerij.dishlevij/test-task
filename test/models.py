from django.db import models


class TestData(models.Model):
    serial_number = models.CharField(max_length=5, unique=True, null=False, blank=False)
    description = models.TextField(max_length=100)
    active = models.BooleanField()
    date = models.DateField()
    csv_file = models.ForeignKey('CSVTable', blank=False, null=False)

    def date_upload(self):
        return CSVTable.objects.filter(pk=self.csv_file.id)[0].date_upload


class CSVTable(models.Model):
    csv_file = models.FileField()
    date_upload = models.DateField(auto_now_add=True)

    def __str__(self):
        return str(self.csv_file)
