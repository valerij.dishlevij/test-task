from django.contrib import admin
from test.models import TestData, CSVTable


class TestDataAdmin(admin.ModelAdmin):
    list_display = ['serial_number', 'description']


class CSVTableAdmin(admin.ModelAdmin):
    list_display = ['csv_file', 'date_upload']

admin.site.register(TestData, TestDataAdmin)
admin.site.register(CSVTable, CSVTableAdmin)