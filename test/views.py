from rest_framework.response import Response
import csv
from django.db import IntegrityError
from datetime import datetime
from test.models import TestData, CSVTable
from test.serializers import ItemListSerializer, FileUploadSerializer, ItemSerializer, RecordByDateSerializer
from rest_framework import viewsets, parsers, mixins


class MultiSerializerMixin(object):
    """Helper to use multiple serializers for different methods."""
    def get_serializer_class(self):
        return self.serializer_class.get(self.action, self.serializers['default'])


class MultiSerializerViewSet(MultiSerializerMixin,
                             mixins.CreateModelMixin,
                             mixins.RetrieveModelMixin,
                             mixins.UpdateModelMixin,
                             mixins.ListModelMixin,
                             viewsets.GenericViewSet):
    serializers = {
        'default': ItemSerializer,
    }


class ItemsView(MultiSerializerViewSet):
    """List and create items. Update active status of the single item."""
    model = TestData
    queryset = TestData.objects.all()
    serializer_class = {
        'list': ItemListSerializer,
        'create': ItemListSerializer,
    }
    lookup_field = 'serial_number'


class FileUploadView(mixins.CreateModelMixin,
                     mixins.ListModelMixin,
                     mixins.RetrieveModelMixin,
                     viewsets.GenericViewSet):
    """Handle CSV uploads. Parse, validate and save items."""

    queryset = CSVTable.objects.all()
    serializer_class = FileUploadSerializer
    parser_classes = (parsers.MultiPartParser, parsers.FormParser,)

    def perform_create(self, serializer):
        self.obj = serializer.save(csv_file=self.request.data.get('csv_file'))

    def create(self, request, *args, **kwargs):
        response = super(FileUploadView, self).create(request, args, kwargs)
        file_name = request.FILES.get('csv_file', None)
        duplicate_snumbers = []
        if file_name is None:
            return response

        try:
            csv_file = csv.reader(file_name)
        except Exception as err:
            csv_file = None
            print 'Read CSV file error: %s' % err

        if csv_file is None:
            return response

        for sn, description, active, date in csv_file:
            try:
                date = datetime.strptime(date, '%Y-%m-%d')
                TestData.objects.create(serial_number=sn, description=description,
                                        active=active, date=date, csv_file=self.obj)
            except IntegrityError as err:
                duplicate_snumbers.append(sn)
                print err
                continue
        if duplicate_snumbers:
            message = [{'Warning': "The follow serial numbers aren't saved, because they already exist."},
                       {'serial_numbers': str(duplicate_snumbers)}]
            return Response(message)
        return response


class RecordByDateView(mixins.ListModelMixin, viewsets.GenericViewSet):
    """Filter records by upload date."""
    serializer_class = RecordByDateSerializer

    def get_queryset(self):
        date_upload = self.kwargs.get('date_upload')
        return TestData.objects.filter(csv_file__date_upload=date_upload)
