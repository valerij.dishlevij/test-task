This is the test project using the following toolset:

* Django
* PostgreSQL
* Swagger documentation
* Django REST Framework


This application is capable of doing the following:

Upload data that is sent to the application via a REST API (see below). The data represents a
CSV file, with the following structure:

* Serialnumber : 5 character string
* Description : 100 character string
* Active : 1 or 0 (indicating True or False)
* Date : ISO8601 date


Store the data in a table in a PostgreSQL database. Serial numbers are globally unique and
proper error handling must be present. If user try to save duplicate serial number the application skip it and then print warning.
The stored data include the contents of the text file as well as a date at which it was uploaded.

Provide a REST API with the following features:

* POST file contents (as described above) to add new records to the database
* Retrieve a set of records uploaded on a given date
* Change the Active status of a given record by serialnumber

Document the API using django_rest_swagger.